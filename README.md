**Transport Plus**
=====================

This is a Vehicle Tracking Application for Transport Plus.

[ Version history ](CHANGELOG.md)

The solution is divided into several projects
---------------------------------------------
### Database
SQL Server Database Project (Right click and publish to deploy)

### DataAccess
for accessing the database. (Entity Framework)

### Model
the business model

### Dto
Data transfer objects for passing data between layers

### Tests
Unit test project

### Web
The UI bit


Development notes
---------------------------------------------
### Set up .NET Core
Seems more stable now and allows us to run on any platform

### Set up MVC with Authentication
Quickly sets up controllers views and built in authentication

### Added SQL Database project
Allows easy deployment and setup of SQL Db

### Added DataAccess layer using EF Framework and Automapper
To allow mapping DB to Model

### Added Dto layer to allow mapping Model to UI

### Added Autofac to use Dependency Injection











 