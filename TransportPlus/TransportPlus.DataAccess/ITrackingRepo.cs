﻿using System;
using System.Collections.Generic;
using TransportPlus.Dto;

namespace TransportPlus.DataAccess
{
    public interface ITrackingRepo
    {
        IEnumerable<TrackingDto> GetAllVehiclesLastKnownLocation();
    }
}
