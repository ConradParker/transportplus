﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TransportPlus.Model;

namespace TransportPlus.DataAccess
{
    public class TransportPlusDbContext : DbContext
    {
        public TransportPlusDbContext(DbContextOptions<TransportPlusDbContext> options) : base(options)
        {
        }

        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Tracking> Tracking { get; set; }
    }
}
