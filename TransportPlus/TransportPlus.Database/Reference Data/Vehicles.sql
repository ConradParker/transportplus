﻿-- Vehicle Data
SET IDENTITY_INSERT [dbo].[Vehicles] ON;
GO

MERGE INTO [dbo].[Vehicles] AS Target
USING (VALUES
    (1, 3451)
	, (2, 2647)
	, (3, 3723)
) AS Source (
	[VehicleId]
	, [TrackingId]
)
ON Target.[Id] = Source.[Id]
-- Update matched rows
WHEN MATCHED THEN
UPDATE SET 
	[TrackingId] = Source.[TrackingId]

-- Insert new rows
WHEN NOT MATCHED BY TARGET THEN
INSERT (
	[VehicleId]
	, [TrackingId]
)
VALUES (
	[VehicleId]
	, [TrackingId]
)

-- Delete rows that are in the target but not the source
WHEN NOT MATCHED BY SOURCE THEN
DELETE;
GO

SET IDENTITY_INSERT [dbo].[Vehicles] OFF;
GO