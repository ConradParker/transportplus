﻿CREATE TABLE [dbo].[Tracking]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [VehicleId] INT NOT NULL, 
    [Latitude] DECIMAL(9, 6) NOT NULL, 
    [Longitude] DECIMAL(9, 6) NOT NULL, 
    [DateAdded] DATETIME NOT NULL,
	CONSTRAINT [FK_TrackingVehicle] FOREIGN KEY ([VehicleId]) REFERENCES [Vehicles]([Id])
)
