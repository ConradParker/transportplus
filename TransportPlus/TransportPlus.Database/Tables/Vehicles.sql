﻿CREATE TABLE [dbo].[Vehicles]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [TrackingId] INT NOT NULL, 
    [DateAdded] DATETIME NOT NULL DEFAULT GETDATE()
)
