﻿using System;

namespace TransportPlus.Dto
{
    public class TrackingDto
    {
        public int Id { get; set; }
        public int VehicleId { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
}
