﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransportPlus.Model
{
    public class Tracking
    {
        public int Id { get; set; }
        public int VehicleId { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public DateTime DateAdded { get; set; }
    }
}
