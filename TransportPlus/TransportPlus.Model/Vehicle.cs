﻿using System;

namespace TransportPlus.Model
{
    public class Vehicle
    {
        public int Id { get; set; }
        public int TrackingId { get; set; }
        public DateTime DateAdded { get; set; }
    }
}
