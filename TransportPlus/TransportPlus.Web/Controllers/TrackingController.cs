﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TransportPlus.DataAccess;
using TransportPlus.Dto;

namespace TransportPlus.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Tracking")]
    public class TrackingController : Controller
    {
        private readonly ITrackingRepo _trackingRepo;

        public TrackingController(ITrackingRepo trackingRepo)
        {
            _trackingRepo = trackingRepo;
        }

        // GET: api/Tracking
        [HttpGet]
        public IEnumerable<TrackingDto> Get()
        {
            return _trackingRepo.GetAllVehiclesLastKnownLocation();
        }
    }
}
