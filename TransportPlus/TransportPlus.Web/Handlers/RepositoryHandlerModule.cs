﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransportPlus.DataAccess;

namespace TransportPlus.Web.Handlers
{
    public class RepositoryHandlerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TrackingRepo>().As<ITrackingRepo>();
        }
    }

}
